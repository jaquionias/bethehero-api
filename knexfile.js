// Update with your config settings.

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: 'semana-omnistack-11',
      user:     'postgres',
      password: 'postgres'
    },
    migrations: {
      directory: "./src/database/migrations",
    }
  },

  test: {
    client: 'postgresql',
    connection: {
      database: 'semana-omnistack-11-test',
      user:     'postgres',
      password: 'postgres'
    },
    migrations: {
      directory: "./src/database/migrations",
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      host : 'postgres-1.cobp2jad4ar0.us-east-2.rds.amazonaws.com',
      database: 'semana-omnistack-11',
      user:     'postgres',
      password: 'gum9Q67iTNEE'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: "./src/database/migrations",
    }
  }

};
