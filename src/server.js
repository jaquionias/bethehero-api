const app = require('./app');

const PORT = 3333;

app.listen(PORT, function () {
    console.log('API escutando na porta ' + PORT);
});